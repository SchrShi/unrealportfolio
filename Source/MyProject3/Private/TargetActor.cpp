// Fill out your copyright notice in the Description page of Project Settings.


#include "TargetActor.h"

#include "Components/TextRenderComponent.h"
#include "Engine/DamageEvents.h"


// Sets default values
ATargetActor::ATargetActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>("Capsule Component");
	SetRootComponent(CapsuleComponent);
	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>("Skeleton");
	SkeletalMesh->SetupAttachment(CapsuleComponent);
	HealthComponent = CreateDefaultSubobject<UHealthComponent>("Health");
	HealthTextRenderComponent = CreateDefaultSubobject<UTextRenderComponent>("Health Text");
	HealthTextRenderComponent->SetupAttachment(CapsuleComponent);
	HealthComponent->OnHealthChange.AddDynamic(this, &ATargetActor::OnHealthChange);
	SetCanBeDamaged(true);
}

// Called when the game starts or when spawned
void ATargetActor::BeginPlay()
{
	Super::BeginPlay();
	//OnHealthChange(HealthComponent->GetHealth());
}

float ATargetActor::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	auto value = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	UE_LOG(LogTemp, Display, TEXT("Target damaged to %f"), value);
	return value;
}

// Called every frame
void ATargetActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATargetActor::OnHealthChange(float NewHealth)
{
	HealthTextRenderComponent->SetText(FText::FromString(FString::Printf(TEXT("%0.f"), NewHealth)));
}

