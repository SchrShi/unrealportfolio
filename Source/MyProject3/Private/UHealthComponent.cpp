// Fill out your copyright notice in the Description page of Project Settings.


#include "UHealthComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogHealthComponent, All, All)

// Sets default values
UHealthComponent::UHealthComponent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryComponentTick.bCanEverTick = false;
	SetHealth(MaxHealth);
}

// Called when the game starts or when spawned
void UHealthComponent::BeginPlay()
{
	GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::OnTakeAnyDamage);
	SetHealth(MaxHealth);
	Super::BeginPlay();
}

bool UHealthComponent::SetHealth(float health)
{
	Health = FMath::Clamp(health, 0.0f, MaxHealth);
	OnHealthChange.Broadcast(Health);
	return true;
}

void UHealthComponent::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType,
	class AController* InstigatedBy, AActor* DamageCauser)
{
	SetHealth(Health - Damage);
	UE_LOG(LogTemp, Display, TEXT("Damage: %f"), Damage);
}

void UHealthComponent::OnTakePointDamage(AActor* DamagedActor, float Damage, AController* InstigatedBy,
	FVector HitLocation, UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection,
	const UDamageType* DamageType, AActor* DamageCauser)
{
	SetHealth(Health - Damage);
	UE_LOG(LogHealthComponent, Display, TEXT("Point damage: %f"), Damage);
}

