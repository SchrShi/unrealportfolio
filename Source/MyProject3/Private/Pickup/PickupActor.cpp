// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickup/PickupActor.h"

#include "Engine/SkeletalMeshSocket.h"

// Sets default values
APickupActor::APickupActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APickupActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickupActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickupActor::Attach()
{
	ChangeCollisionEnable(false);
}

void APickupActor::Deattach()
{
	ChangeCollisionEnable(true);
}

void APickupActor::Attack_To_Socket(USkeletalMeshComponent* character_mesh, FName socket_name)
{
	ChangeCollisionEnable(false);

	if(const USkeletalMeshSocket *pickup_socket = character_mesh->GetSocketByName(socket_name))
	{
		pickup_socket->AttachActor(this, character_mesh);
		UE_LOG(LogTemp, Display, TEXT("Attach actor"));
	}
	else
	{
		UE_LOG(LogTemp, Display, TEXT("Cant find socket with name %s"), *socket_name.ToString());
	}
}


void APickupActor::ChangeCollisionEnable(bool isEnable)
{
	USceneComponent *root_component = GetRootComponent();

	if(UPrimitiveComponent *prim_component = Cast<UPrimitiveComponent>(root_component))
	{
		prim_component->SetSimulatePhysics(isEnable);
		prim_component->SetCollisionProfileName(isEnable ? UCollisionProfile::PhysicsActor_ProfileName : UCollisionProfile::NoCollision_ProfileName);
	}
}

