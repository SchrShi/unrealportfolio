#include "MyProject3/Public/PlayerPawn.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "MyPlayerController.h"
#include "Components/ArrowComponent.h"
#include "Components/TextRenderComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "Engine/DamageEvents.h"
#include "Kismet/GameplayStatics.h"

APlayerPawn::APlayerPawn()
{
	PrimaryActorTick.bCanEverTick = true;

	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleCollider"));
	check(CapsuleComponent != nullptr);

	SetRootComponent(CapsuleComponent);

	CapsuleComponent->SetSimulatePhysics(true);
	
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	check(SpringArmComponent != nullptr);

	SpringArmComponent->SetupAttachment(CapsuleComponent);
	
	FPSCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	check(FPSCameraComponent != nullptr);
	
	FPSCameraComponent->SetupAttachment(SpringArmComponent);

	ThirdPersonSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	check(ThirdPersonSkeletalMesh != nullptr);
	ThirdPersonSkeletalMesh->SetupAttachment(CapsuleComponent);

	ThirdPersonSkeletalMesh->SetCastShadow(false);

	FirstPersonSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("First person Skeletal Mesh"));
	check(FirstPersonSkeletalMesh != nullptr);
	FirstPersonSkeletalMesh->SetupAttachment(FPSCameraComponent);

	FirstPersonSkeletalMesh->SetCastShadow(false);

	ShadowSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Shadow cast mesh"));
	ShadowSkeletalMesh->SetupAttachment(CapsuleComponent);

	WeaponParentPoint = CreateDefaultSubobject<USceneComponent>(TEXT("WeaponParentPoint"));

	check(WeaponParentPoint != nullptr);
	WeaponParentPoint->SetupAttachment(CapsuleComponent);

	FirstPersonSkeletalMesh->SetVisibility(true);
	ThirdPersonSkeletalMesh->SetVisibility(false);

	Movement = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("Movement"));

	CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health"));
	HealthTextRenderComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("Health Text"));
	HealthTextRenderComponent->SetupAttachment(GetRootComponent());

	HealthComponent->OnHealthChange.AddDynamic(this, &APlayerPawn::OnHeathChange);
}

void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	HasGun = false;
	FirstPersonSkeletalMesh->SetVisibility(true);
	ThirdPersonSkeletalMesh->SetVisibility(false);

	//OnHeathChange(HealthComponent->GetHealth());
}

bool APlayerPawn::CanJump()
{
	FHitResult Hit;
	
	FVector TraceStart = GetActorLocation();
	FVector TraceEnd = GetActorLocation() - GetActorUpVector() * (CapsuleComponent->GetScaledCapsuleHalfHeight() + AdditiveHeightTrace);
	
	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(this);
	
	GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceEnd, TraceChannelProperty, QueryParams);
	//DrawDebugLine(GetWorld(), TraceStart, TraceEnd, Hit.bBlockingHit ? FColor::Blue : FColor::Red, true, 10);
	
	return Hit.bBlockingHit;
}

void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	InAir = !CanJump();
	CapsuleComponent->SetPhysicsAngularVelocityInRadians(FVector::Zero());
}

void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	UEnhancedInputComponent* ic = Cast<UEnhancedInputComponent>(PlayerInputComponent);
	AMyPlayerController* PlayerController = Cast<AMyPlayerController>(Controller);
	check(ic && PlayerController)
	
	ic->BindAction(PlayerController->MovingAction, ETriggerEvent::Triggered, this, &APlayerPawn::Move);
	ic->BindAction(PlayerController->MovingAction, ETriggerEvent::Completed, this, &APlayerPawn::StopMove);
	ic->BindAction(PlayerController->RotateAction, ETriggerEvent::Triggered, this, &APlayerPawn::Rotate);
	ic->BindAction(PlayerController->FireAction, ETriggerEvent::Triggered, this, &APlayerPawn::Fire);
	ic->BindAction(PlayerController->PickupAction, ETriggerEvent::Started, this, &APlayerPawn::Pickup);
	ic->BindAction(PlayerController->FieldOfViewAction, ETriggerEvent::Triggered, this, &APlayerPawn::ChangeField);

	ULocalPlayer* LocalPlayer = PlayerController->GetLocalPlayer();
	check(LocalPlayer);

	UEnhancedInputLocalPlayerSubsystem* Subsystem = LocalPlayer->GetSubsystem<UEnhancedInputLocalPlayerSubsystem>();
	check(Subsystem);
	Subsystem->ClearAllMappings();
	Subsystem->AddMappingContext(PlayerController->PawnMappingContext, 0);
}

void APlayerPawn::Move(const FInputActionValue& ActionValue)
{
	FVector input = ActionValue.Get<FInputActionValue::Axis3D>();
	LastInput = input;

	if(!InAir)
	{
		if(input.Z > 0)
		{
			CapsuleComponent->AddImpulse(GetActorUpVector() * JumpForce);
			IsJump = true;
		}
		else
		{
			IsJump = false;
		}
	}
	else
	{
		IsJump = false;
	}

	input.Z = 0;
	AddMovementInput(GetActorRotation().RotateVector(input), WalkSpeedScale);
}

void APlayerPawn::StopMove(const FInputActionValue& ActionValue)
{
	LastInput = FVector(0,0,0);
	IsJump = false;
}

void APlayerPawn::Rotate(const FInputActionValue& ActionValue)
{
	FRotator ActorInput(ActionValue[0], ActionValue[1], ActionValue[2]);
	ActorInput *= GetWorld()->GetDeltaSeconds() * RotateSpeedScale;
	ActorInput += GetActorRotation();

	ActorInput.Pitch = 0;
	ActorInput.Roll = 0;
	
	SetActorRotation(ActorInput);
	
	FRotator CameraInput(ActionValue[0], ActionValue[1], ActionValue[2]);
	
	CameraInput *= GetWorld() -> GetDeltaSeconds() * RotateSpeedScale;
	
	CameraInput += SpringArmComponent->GetComponentRotation();
	CameraInput.Pitch = FMath::Clamp(CameraInput.Pitch, -89.9f, 89.9f);
	CameraInput.Yaw = ActorInput.Yaw;
	CameraInput.Roll = 0;
	
	SpringArmComponent->SetWorldRotation(CameraInput);
}

void APlayerPawn::Fire(const FInputActionValue& ActionValue)
{
	if(CurrentWeapon != nullptr)
	{
		CurrentWeapon->Fire(this);
	}
}

void APlayerPawn::Pickup(const FInputActionValue& ActionValue)
{
	FHitResult Hit;
	
	FVector TraceStart = GetActorLocation();
	FVector TraceEnd = GetActorLocation() + FPSCameraComponent->GetForwardVector() * MaxLengthToGrap;

	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(this);
	
	GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceEnd, WeaponChannel, QueryParams);

	if(Hit.bBlockingHit && Hit.GetActor() != nullptr)
	{
		if(AWeapon *pickup = Cast<AWeapon>(Hit.GetActor()))
		{
			if(pickup == nullptr)
			{
				return;
			}
			
			 if(CurrentWeapon != nullptr)
			 {
			// 	CurrentWeapon->Deattach();
			 	CurrentWeapon->DetachFromActor(FDetachmentTransformRules::KeepRelativeTransform);
			 	CurrentWeapon->Deattach();
			 }
			
			CurrentWeapon = pickup;
			CurrentWeapon->Attach();
			const FAttachmentTransformRules rule = FAttachmentTransformRules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepRelative, true);
			CurrentWeapon->AttachToComponent(WeaponParentPoint, rule);
			//CurrentWeapon->GetTransform().TransformPosition(UE::Math::TVector<double>::Zero());
			//CurrentWeapon->SetActorLocationAndRotation(WeaponPoint->GetComponentTransform().GetLocation(), WeaponPoint->GetComponentTransform().GetRotation());
			HasGun = true;
			if(IsFirstPersonView)
			{
				pickup->Attack_To_Socket(FirstPersonSkeletalMesh, WeaponSocketName);
			}
			else
			{
				pickup->Attack_To_Socket(ThirdPersonSkeletalMesh, WeaponSocketName);
			}
			ThirdPersonSkeletalMesh->GetAnimInstance()->Montage_Play(HasRifleAnimMontage);
			FirstPersonSkeletalMesh->GetAnimInstance()->Montage_Play(HasRifleAnimMontage);
		}
	}
}

void APlayerPawn::ChangeField(const FInputActionValue& ActionValue)
{
	float additiveValue = ActionValue.Get<float>();
	float currentValue = SpringArmComponent->TargetArmLength;
	
	if(IsFirstPersonView)
	{
		if(additiveValue > 0)
		{
			SpringArmComponent->TargetArmLength = FOVMinValue;
			IsFirstPersonView = false;
			FirstPersonSkeletalMesh->SetVisibility(false);
			ThirdPersonSkeletalMesh->SetVisibility(true);
			if(CurrentWeapon != nullptr)
			{
				CurrentWeapon->Deattach();
				CurrentWeapon->Attack_To_Socket(ThirdPersonSkeletalMesh, WeaponSocketName);
			}

			SpringArmComponent->SetRelativeLocation(ThirdPersonCameraPosition);
			UE_LOG(LogTemp, Display, TEXT("Set Third Person %s"), *SpringArmComponent->GetRelativeLocation().ToString());
		}
		return;
	}
	
	
	if(!IsFirstPersonView && currentValue + additiveValue <= FOVMinValue)
	{
		IsFirstPersonView = true;
		SpringArmComponent->TargetArmLength = FOVFirstPerson;
		SpringArmComponent->SetRelativeLocation(FirstPersonCameraPosition);
		FirstPersonSkeletalMesh->SetVisibility(true);
		if(CurrentWeapon != nullptr)
		{
			CurrentWeapon->Deattach();
			CurrentWeapon->Attack_To_Socket(FirstPersonSkeletalMesh, WeaponSocketName);
		}
		ThirdPersonSkeletalMesh->SetVisibility(false);

		for(auto& str : HideBones)
		{
			const auto index = FirstPersonSkeletalMesh->GetBoneIndex(FName(*str));
			FirstPersonSkeletalMesh->HideBone(index, PBO_None);
		}
		
		UE_LOG(LogTemp, Display, TEXT("Set First Person %s"), *SpringArmComponent->GetRelativeLocation().ToString());
		
		return;
	}
	
	SpringArmComponent->TargetArmLength = FMath::Clamp(currentValue + additiveValue * ChangeFOVSensitivity, FOVMinValue, FOVMaxValue);
}

FTransform APlayerPawn::GetRightHandPosition()
{
	if(CurrentWeapon != nullptr)
	{
		if(CurrentWeapon->WeaponMeshComponent->DoesSocketExist(CurrentWeapon->RightHandSocket))
		{
			return CurrentWeapon->WeaponMeshComponent->GetSocketTransform(CurrentWeapon->RightHandSocket);
		}
		UE_LOG(LogTemp, Display, TEXT("Not has socket with name %s"), *CurrentWeapon->RightHandSocket.ToString());
	}

	return {};
}

FTransform APlayerPawn::GetLeftHandPosition()
{
	if(CurrentWeapon != nullptr)
	{
		if(CurrentWeapon->WeaponMeshComponent->DoesSocketExist(CurrentWeapon->LeftHandSocket))
		{
			return CurrentWeapon->WeaponMeshComponent->GetSocketTransform(CurrentWeapon->LeftHandSocket);
		}
		UE_LOG(LogTemp, Display, TEXT("Not has socket with name %s"), *CurrentWeapon->LeftHandSocket.ToString());
	}
	
	return {};
}

void APlayerPawn::OnHeathChange(float NewHealth)
{
	HealthTextRenderComponent->SetText(FText::FromString(FString::Printf(TEXT("%0.f"), NewHealth)));
}



