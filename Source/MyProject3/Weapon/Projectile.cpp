// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"

#include "Components/ArrowComponent.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Collider = CreateDefaultSubobject<USphereComponent>(TEXT("Collider"));

	SetRootComponent(Collider);

	Collider->BodyInstance.SetCollisionProfileName("Projectile");
	Collider->SetSimulatePhysics(true);
	Collider->OnComponentHit.AddDynamic(this, &AProjectile::OnHit);

	Collider->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0));
	Collider->CanCharacterStepUpOn = ECB_No;

	Movement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	Movement->UpdatedComponent = Collider;
	Movement->bRotationFollowsVelocity = true;
	Movement->bShouldBounce = false;
	Movement->InitialSpeed = Movement->MaxSpeed;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Collider);

	CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));

	InitialLifeSpan = 3;

	ImpulseScale = 0.1f;
	OnHitToActor = FProjectileHitToActor();
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	Movement->InitialSpeed = Movement->MaxSpeed;
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectile::OnHit(UPrimitiveComponent* ownerHitComponent, AActor* actor, UPrimitiveComponent* otherHitComponent,
	UE::Math::TVector<double> normalImpulse, const FHitResult& result)
{
	if((otherHitComponent != nullptr) && (otherHitComponent != ownerHitComponent) && (otherHitComponent != nullptr) && otherHitComponent->IsSimulatingPhysics())
	{
		otherHitComponent->AddImpulseAtLocation(GetVelocity() * ImpulseScale, GetActorLocation());
		
		UE_LOG(LogTemp, Display, TEXT("Projectile: %s"), *actor->GetName())

		OnHitToActor.Broadcast(actor);
		//TODO AC: Decal
		
		Destroy();
	}
}

