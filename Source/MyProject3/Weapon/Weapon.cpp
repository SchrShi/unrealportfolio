#include "Weapon.h"
#include "MyPlayerController.h"
#include "Projectile.h"
#include "Components/ArrowComponent.h"
#include "Engine/DamageEvents.h"
#include "Kismet/GameplayStatics.h"

AWeapon::AWeapon()
{
	WeaponMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(FName(TEXT("Mesh")));
	Box = CreateDefaultSubobject<UBoxComponent>(FName(TEXT("Box")));
	SetRootComponent(Box);
	WeaponMeshComponent->SetupAttachment(Box);
	MuzzleOffset = FVector(0, 0, 10);
	CreateDefaultSubobject<UArrowComponent>(FName(TEXT("Arrow")));
}

void AWeapon::Fire(APawn *character)
{
	if(character == nullptr || character->GetController() == nullptr)
	{
		UE_LOG(LogTemp, Display, TEXT("Character is null"));
		return;
	}

	if(Projectile == nullptr)
	{
		UE_LOG(LogTemp, Display, TEXT("Projectile is null"));
		return;
	}

	if(UWorld *world = GetWorld())
	{
		auto time = world->GetTime().GetRealTimeSeconds();

		auto waitTime = time - LastShootTime;
		if(waitTime < 1 / FireRate)
		{
			return;
		}

		LastShootTime = time;
		
		APlayerController *playerController = Cast<AMyPlayerController>(character->GetController());

		LastOwnerController = character->GetController();
		FRotator spawnRotation = playerController->PlayerCameraManager->GetCameraRotation();
		FVector spawnPosition = GetActorLocation() + spawnRotation.RotateVector(MuzzleOffset);

		FActorSpawnParameters actorSpawnParamers;

		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetTransform().GetLocation());

		actorSpawnParamers.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
		auto projectile = world->SpawnActor<AProjectile>(Projectile, spawnPosition, spawnRotation, actorSpawnParamers);
		projectile->OnHitToActor.AddDynamic(this, &AWeapon::OnHitToActor);
	}
}

void AWeapon::Attach()
{
	Super::Attach();
}

void AWeapon::Deattach()
{
	Super::Deattach();
	LastOwnerController = nullptr;
}

void AWeapon::Attack_To_Socket(USkeletalMeshComponent* character_mesh, FName socket_name)
{
	Super::Attack_To_Socket(character_mesh, socket_name);
}

void AWeapon::OnHitToActor(AActor* actor)
{
	if(LastOwnerController)
	{
		auto actualDamage = actor->TakeDamage(Damage, FDamageEvent(UDamageType::StaticClass()), LastOwnerController, this);
		UE_LOG(LogTemp, Display, TEXT("Weapon damaged %s to %f damage"), *actor->GetName(), actualDamage);
	}
}
