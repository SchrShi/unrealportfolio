#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "Pickup/PickupActor.h"
#include "Weapon.generated.h"


UCLASS()
class MYPROJECT3_API AWeapon : public APickupActor
{
	GENERATED_BODY()

	AWeapon();

public:
	UFUNCTION(BlueprintCallable, Category="Weapon")
	virtual void Fire(APawn *character);
	
	virtual void Attach() override;
	
	virtual void Deattach() override;

	virtual void Attack_To_Socket(USkeletalMeshComponent *character_mesh, FName socket_name) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon|GamePlay")
	FVector MuzzleOffset;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon|GamePlay")
	float FireRate;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon|GamePlay")
	USoundBase *FireSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon|GamePlay")
	UAnimationAsset *FireAnimation;
	UPROPERTY(EditAnywhere)
	USkeletalMeshComponent *WeaponMeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon|Settings")
	FName LeftHandSocket;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon|Settings")
	FName RightHandSocket;
 
	UPROPERTY(EditAnywhere)
	UBoxComponent *Box;
	
	UPROPERTY(EditDefaultsOnly, Category="Weapon|Projectile")
	TSubclassOf<class AProjectile> Projectile;

	UPROPERTY()
	double LastShootTime;

protected:
	UPROPERTY(EditAnywhere)
	float Damage;

private:
	UFUNCTION()
	void OnHitToActor(AActor* actor);

	AController* LastOwnerController;
};
