// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Projectile.generated.h"

UDELEGATE()
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FProjectileHitToActor, AActor*, Actor);

UCLASS()
class MYPROJECT3_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	AProjectile();

	UPROPERTY(BlueprintAssignable)
	FProjectileHitToActor OnHitToActor;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnHit(UPrimitiveComponent *ownerHitComponent, AActor *actor, UPrimitiveComponent *otherHitComponent, FVector normalImpulse, const FHitResult& result);


	UPROPERTY(EditAnywhere)
	USphereComponent *Collider;

	UPROPERTY(EditAnywhere)
	UProjectileMovementComponent *Movement;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent *Mesh;

	UPROPERTY(BlueprintReadWrite)
	float ImpulseScale;
};
