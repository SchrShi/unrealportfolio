#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "UHealthComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/SpringArmComponent.h"
#include "MyProject3/Weapon/Weapon.h"
#include "PlayerPawn.generated.h"

class UTextRenderComponent;

UCLASS()
class MYPROJECT3_API APlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	APlayerPawn();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category="Health")
	class UHealthComponent* HealthComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Health")
	UTextRenderComponent* HealthTextRenderComponent;

	UPROPERTY(EditAnywhere, Category="Field Of View")
	FVector FirstPersonCameraPosition;

	UPROPERTY(EditAnywhere, Category="Field Of View")
	FVector ThirdPersonCameraPosition;
	
private:
	bool CanJump();

public:	
	virtual void Tick(float DeltaTime) override;
	
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	void Move(const struct FInputActionValue& ActionValue);

	void StopMove(const FInputActionValue& ActionValue);

	void Rotate(const FInputActionValue& ActionValue);

	void Fire(const FInputActionValue& ActionValue);

	void Pickup(const FInputActionValue& ActionValue);

	void ChangeField(const FInputActionValue& ActionValue);

	UFUNCTION(BlueprintCallable)
	FTransform GetRightHandPosition();

	UFUNCTION(BlueprintCallable)
	FTransform GetLeftHandPosition();

	UPROPERTY(BlueprintReadOnly, Category="Weapon")
	AWeapon *CurrentWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon")
	FName WeaponSocketName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon")
	TEnumAsByte<ECollisionChannel> WeaponChannel = ECC_WorldDynamic;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon")
	UAnimMontage* HasRifleAnimMontage;
 
	UPROPERTY(EditAnywhere)
	UCameraComponent* FPSCameraComponent;

	UPROPERTY(EditAnywhere)
	USpringArmComponent* SpringArmComponent;

	UPROPERTY(EditAnywhere)
	UCapsuleComponent* CapsuleComponent;

	UPROPERTY(EditAnywhere)
	USkeletalMeshComponent* ThirdPersonSkeletalMesh;

	UPROPERTY(EditAnywhere)
	USkeletalMeshComponent* FirstPersonSkeletalMesh;

	UPROPERTY(EditAnywhere)
	USkeletalMeshComponent* ShadowSkeletalMesh;

	UPROPERTY(EditAnywhere)
	UFloatingPawnMovement* Movement;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USceneComponent* WeaponParentPoint;

	UPROPERTY(EditAnywhere, Category="Movement")
	float WalkSpeedScale = 1.0f;

	UPROPERTY(EditAnywhere, Category="Movement")
	float RotateSpeedScale = 30.0f;

	UPROPERTY(EditAnywhere, Category="Movement|Jump")
	TEnumAsByte<ECollisionChannel> TraceChannelProperty = ECC_WorldStatic;

	UPROPERTY(EditAnywhere, Category="Movement|Jump")
	float AdditiveHeightTrace = 0.1f;

	UPROPERTY(EditAnywhere, Category="Movement|Jump")
	float JumpForce = 20000;

	UPROPERTY(EditAnywhere, Category="Movement|Jump")
	UAnimMontage* FallAnim;

	UPROPERTY(EditAnywhere, Category="Movement|Jump")
	UAnimMontage* JumpAnim;

	UPROPERTY(EditAnywhere, Category="Pickup")
	float MaxLengthToGrap;

	UPROPERTY(EditAnywhere, Category="Field Of View")
	float FOVMinValue = 60;

	UPROPERTY(EditAnywhere, Category="Field Of View")
	float FOVMaxValue = 200;

	UPROPERTY(EditAnywhere, Category="Field Of View")
	float FOVFirstPerson = -30;

	UPROPERTY(EditAnywhere, Category="Field Of View")
	float ChangeFOVSensitivity = 10;

	UPROPERTY(EditAnywhere)
	bool IsFirstPersonView = true;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	FVector LastInput;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	bool InAir;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	bool IsJump;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool HasGun;

	UPROPERTY(EditAnywhere, Category="Field OF View")
	TArray<FString> HideBones;

	void OnHeathChange(float NewHealth);
};