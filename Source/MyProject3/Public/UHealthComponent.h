// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UHealthComponent.generated.h"


UDELEGATE()
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHealthOnChange, float, NewHealth);

UCLASS(ClassGroup=(Custom), meta = (BlueprintSpawnableComponent))
class MYPROJECT3_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	UHealthComponent();

	UPROPERTY(BlueprintAssignable)
	FHealthOnChange OnHealthChange;

	UFUNCTION(BlueprintCallable)
	float GetHealth() const {return Health;}

	UFUNCTION(BlueprintCallable)
	float GetHealthPercent() const {return Health / MaxHealth;}

	UFUNCTION()
	void OnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType,
	class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION()
	void OnTakePointDamage(AActor* DamagedActor, float Damage, class AController* InstigatedBy, FVector HitLocation, class UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const class UDamageType* DamageType, AActor* DamageCauser);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Health", meta=(ClampMin = "1.0"))
	float MaxHealth = 100.0f;

private:
	float Health = 0.0f;

	bool SetHealth(float health);
};
